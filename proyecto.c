#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <omp.h>
#include <time.h>

#define tamanio 16
#define numCompuestos 12422

typedef struct{
    char identif[13];
    int compContado[tamanio];

}compuesto;

int *contarElementos (char *cadena){

	static int compContd[tamanio];//ojo
	int i=0;
	for(;i<16;i++){
        compContd[i]=0;
	}

	int j=0,patron[]={'@','c','C','F','H','I','N','O','P','S','B','l','n','o','r','s'};

    while(*cadena != '\0'){
            //printf("hola");
        for(j=0;j<16;j++){
            if(*cadena==patron[j]){
                compContd[j]++;
                break;
            }
        }
        cadena++;
    }
	return compContd;
}
//calcula el coeficiente tanimoto
float hallarCoeficienteTanimoto (int *filas, int *colum){
    int na, nb, nc, a, b, c;
    na= nb= nc= a= b= c =0;
    int acumulador=0;
    while(acumulador<tamanio){
        if(*filas>0 || *colum>0){
            if(acumulador==0){
                if(*filas>0 && *colum>0){
                    a= b= c =1;
                }else if(*filas>0){
                    a=1;
                }else{
                    b=1;
                }
            }else{
                if(*filas > *colum){
                    nc = nc + *colum;
                }else{
                    nc = nc + *filas;
                }
                na = na + *filas;
                nb = nb + *colum;
            }
        }
        acumulador++;
        filas++;
        colum++;
    }
    na = na + a;
    nb = nb + b;
    nc = nc + c;

    return (float)nc / (float)(na + nb - nc);
}

int main()
{
    FILE *archivoEntrada, *archivoSalida;

    char cadena [200];
    static char identificador[]="ccccccccccccc";
    char c;
    int posicion, *p, i, x=0, y=0, cont;
    float coeficiente;

    i=0;coeficiente=0.0;

    clock_t start=clock();
    archivoEntrada = fopen("ZINC_chemicals.tsv", "r");

    compuesto comp;
    compuesto arregloCompuestos [numCompuestos];
    while(1){
//recorre las columnas hasta llegar a un cambio de linea
        c = fgetc(archivoEntrada);
        if(c=='\t'){
            posicion=0;
            do{
                identificador[posicion]=c;
                posicion++;
                c = fgetc(archivoEntrada);
            }while(c != '\t');

            identificador[posicion] = '\0';
            do{
                c = fgetc(archivoEntrada);
            }while(c != '\t');
            posicion=0;
            do{
                c = fgetc(archivoEntrada);
                cadena[posicion] = c;
                posicion++;
            }while(c != '\n');
            cadena[posicion]='\0';

            //cuenta letras y guarda todo en estruct de Compuesto
            p = contarElementos(cadena);
            posicion=0;

            while(posicion<tamanio){
                comp.compContado[posicion] = *p;
                p++;
                posicion++;
            }
            strcpy(comp.identif, identificador);

            arregloCompuestos[i++]=comp;
        }
        if(feof(archivoEntrada)){
            break;
        }
    }
    fclose(archivoEntrada);

    archivoSalida = fopen("ResultadosCoeficientes.txt","w");

    //start=clock();
    omp_set_num_threads(omp_get_max_threads());

    //proceso paralelizado
    #pragma omp parallel for ordered
    for(x=0; x<numCompuestos; x++){
        for(y=x+1; y<numCompuestos; y++){
            coeficiente = hallarCoeficienteTanimoto(arregloCompuestos[x].compContado, arregloCompuestos[y].compContado);
            #pragma omp ordered
//se escribe en el archivoSalida
            fprintf(archivoSalida, "%s\t%s\t%.2f\n",arregloCompuestos[x].identif, arregloCompuestos[y].identif, coeficiente);
        }
    }
    fclose(archivoSalida);


    return 0;

}
