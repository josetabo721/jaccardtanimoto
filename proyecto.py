def eliminarArrobasConsecutivas(a):
    lista=[]
    lista.append(a[0])
    for i in range(1,len(a)):
        if (lista[-1]!='@' or a[i]!='@'):
            lista.append(a[i])
    
    return lista

def obtenerVecesQueSeRepiteUnElemento(lista,elemento):
    contador=0
    for elementos in lista:
        if(elementos==elemento):
            contador=contador+1
            
    return contador
    

def obtenerListaDeElementosSinRepeticion(lista):
    elementosSinRepeticion=[]
    for i in range(0,len(lista)):
        if(obtenerVecesQueSeRepiteUnElemento(elementosSinRepeticion,lista[i])==0 and lista[i]!='(' and lista[i] !=')' and lista[i]!='[' and lista[i] !=']' and lista[i]!='=' and lista[i] !='-' and lista[i] !='+' and not lista[i].isdigit() and lista[i] !='n' ):
            elementosSinRepeticion.append(lista[i])
    return elementosSinRepeticion

def hallarNroDeElementos(lista):
    nroElementos=0;
    elementosSinRepeticion=obtenerListaDeElementosSinRepeticion(lista)
    for i in range(0,len(elementosSinRepeticion)):
        nroElementos=nroElementos+obtenerVecesQueSeRepiteUnElemento(lista, elementosSinRepeticion[i])
    
    return nroElementos

def hallarNroDeElementosComunes(lista1,lista2):
    nroElementosComunes=0;
    elementosSinRepeticion=obtenerListaDeElementosSinRepeticion(lista1)
    for i in range(0,len(elementosSinRepeticion)):
        nroElementosComunes=nroElementosComunes+min(obtenerVecesQueSeRepiteUnElemento(lista1, elementosSinRepeticion[i]),obtenerVecesQueSeRepiteUnElemento(lista2, elementosSinRepeticion[i]))
    return nroElementosComunes

def hallarIndiceJaccardTanimoto(cadena1,cadena2):
    cadena1=cadena1.replace("Cl","*").replace("Br","$")
    cadena2=cadena2.replace("Cl","*").replace("Br","$")
    Na=hallarNroDeElementos(eliminarArrobasConsecutivas(cadena1))
    Nb=hallarNroDeElementos(eliminarArrobasConsecutivas(cadena2))
    Nc=hallarNroDeElementosComunes(eliminarArrobasConsecutivas(cadena1), eliminarArrobasConsecutivas(cadena2))
    print (Na,Nb,Nc)
    return float(Nc)/float(Na+Nb-Nc)


'''
INCIO
'''

indicadores=[];
keys=[];
archivoEntrada = open("ZINC_chemicals.tsv", "r") 
for i,linea in enumerate(archivoEntrada):
    if i>=1 and i<10:
        indicadores.append(linea[linea.find('\t')+1:linea.find('\t')+13])
        keys.append(linea[linea.rfind('\t')+1:len(linea)-1])

archivoEntrada.close()


archivoSalida=open("resutados.txt","w");
for i in range(0,len(keys)-1):
    for j in range(0,len(keys)-1):
        if (i<j):
            archivoSalida.write(indicadores[i]+'\t'+indicadores[j]+'\t'+"%.2f"%hallarIndiceJaccardTanimoto(keys[i], keys[j])+'\n')          
